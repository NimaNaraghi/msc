The literature reviews

Owing to the tremendous growth of internet, it was predicted that disintermediation in travel industry is indefinite within a brief time. In 1998, a research conducted by Walden and Anckar (2006) uncovered that the predictions were overestimating the impacts of internet on travel domain because self-booking demanded too much effort to make it a menace to the traditional intermediates. They repeated the same research in 2005 to investigate whether the recent developments of internet and ICT had been adequate to substitute the traditional travel agencies. However, it revealed that self-booking was still required a significant effort due to insufficient advances in e-commerce to propose a real value which ended to massive failures among the engaged operations. Also, according to Anckar and Walden (2001) they studied the problems that the users faced by categorizing them into low-complexity and high-complexity self-booking whilst the results showed that the users must cope with similar problems in both categories.



Resources
Anckar, B. and Walden, P., 2001. Self-booking of high-and low-complexity travel products: exploratory findings. Information Technology & Tourism, 4(3-1), pp.151-165.
ELHAJ, M., 2011. PREFERENCES FOR ONLINE Vs. TRADITIONAL TRAVEL RESERVATION. ASEAN Journal on Hospitality and Tourism, 10(2), pp.100-109.
Walden, P. and Anckar, B., 2006, January. A reassessment of the efficacy of self-booking in travel. In System Sciences, 2006. HICSS'06. Proceedings of the 39th Annual Hawaii International Conference on (Vol. 6, pp. 132b-132b). IEEE.
Xiang, Z., Wang, D., O’Leary, J.T. and Fesenmaier, D.R., 2015. Adapting to the internet: trends in travelers’ use of the web for trip planning. Journal of Travel Research, 54(4), pp.511-527.
